﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StairTop : MonoBehaviour
{
    public GameObject floor;
    public float scale;
    public Transform topPosition;
    public Transform topPosition2;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            floor.transform.localScale = new Vector3(floor.transform.localScale.x, scale, floor.transform.localScale.z);

            //collision.gameObject.GetComponent<SpriteRenderer>().sortingOrder++;

            if (gameObject.tag == "StairTop_TopPart")
            {
                collision.gameObject.GetComponent<PlayerController>().isGrounded = true;

                collision.gameObject.transform.position = topPosition2.position;
            }
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (gameObject.tag == "StairTop_TopPart" && collision.gameObject.tag == "Player")
        {

            if (collision.gameObject.GetComponent<PlayerController>().IsGoingDown())
            {

                collision.gameObject.transform.position = topPosition.position;

            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {

            //collision.gameObject.GetComponent<SpriteRenderer>().sortingOrder--;

        }
    }

}