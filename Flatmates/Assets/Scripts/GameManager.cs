﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public bool isFinished = false;
    public GameObject endScreen1;
    public GameObject endScreen2;

    public GameObject a;

    public AudioSource[] myAudio;
    public AudioSource[] audioA;
    public AudioSource[] audioB;
    public AudioSource[] audioC;
    public AudioSource[] toUse;

    // Start is called before the first frame update
    void Start()
    {
        a.SetActive(false);
        endScreen1.SetActive(false);
        endScreen2.SetActive(false);

        myAudio = GetComponents<AudioSource>();

        audioA = new AudioSource[3];
        audioA[0] = myAudio[0];
        audioA[1] = myAudio[1];
        audioA[2] = myAudio[2];

        audioB = new AudioSource[3];
        audioB[0] = myAudio[3];
        audioB[1] = myAudio[4];
        audioB[2] = myAudio[5];

        audioC = new AudioSource[3];
        audioC[0] = myAudio[5];
        audioC[1] = myAudio[6];
        audioC[2] = myAudio[7];


        int x = Random.Range(1, 3);
        if (x == 1)
        {
            toUse = audioA;
        }
        if (x == 2)
        {
            toUse = audioB;
        }
        if (x == 3)
        {
            toUse = audioC;
        }

        for(int i=0;i< toUse.Length;i++)
        {
            if(toUse[i])
                toUse[i].Play();
        }

    }

    // Update is called once per frame
    void Update()
    {
        if(isFinished)
        {
            bool a = Input.GetButton("Action1") || Input.GetButton("Action2") || Input.GetKeyDown(KeyCode.G) || Input.GetKeyDown(KeyCode.Space);
            if (a)
                ResetGame();

        }
    }

    public void GameOver(int player)
    {
        Invoke("Finish", 3f);
       //a.SetActive(true);
        if(player == 1)
            endScreen1.SetActive(true);
        if (player == 2)
            endScreen2.SetActive(true);

    }

    public void Finish()
    {
        isFinished = true;

    }

    public void ResetGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
