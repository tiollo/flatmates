﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProgressBar : MonoBehaviour {

    private Image bar;
    private Color normalColor;
    public Color endColor;
    public GameManager gameManager;

    private float nextSize = 0f;
    private float speed = 0.3f;
    private float resetSpeed = 3f;
    private bool straightReset = false;
    private RectTransform t;

    private bool isLevelCompleted = false;

    // Use this for initialization
    void Start () {
        bar = GetComponent<Image>();
        normalColor = bar.color;

        t = GetComponent<RectTransform>();
	}
	
	// Update is called once per frame
	void Update () {

        //if(!isLevelCompleted)
        {
            if (nextSize > 0 && nextSize <= 1f)
            {
                //print("nextsize=" + nextSize + " scalex=" + GetComponent<RectTransform>().localScale.x);
                // if (t.localScale.x <= nextSize)
                {
                    if (nextSize > t.localScale.x)
                    {
                        float v = t.localScale.x + Time.deltaTime * speed;
                        //v = Mathf.Clamp(v, nextSize, 1);
                        t.localScale = new Vector3(v, 1, 1);

                    }
                    else if (nextSize < t.localScale.x)
                    {
                        float v = t.localScale.x - Time.deltaTime * speed;
                        //v = Mathf.Clamp(v, 0, nextSize);
                        t.localScale = new Vector3(v, 1, 1);
                    }
                    else
                    {
                        t.localScale = new Vector3(nextSize, 1, 1);
                    }
                }


            }
            if (t.localScale.x >= 1f)
            {
               // isLevelCompleted = true;
            }
        }

    }

    public void UpdateBar(int current, int goal)
    {

        float percentage = current / (float)goal;
        if (percentage >= 1f)
            percentage = 1f;

        nextSize = percentage;
    }


}
