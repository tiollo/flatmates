﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fire : MonoBehaviour
{
    public bool isOn = false;
    public Transform pos;

    public ParticleSystem p1;
    public ParticleSystem p2;

    // Start is called before the first frame update
    void Start()
    {
        p1.Stop();
        if(p2)
            p2.Stop();
        //GetComponent<SpriteRenderer>().enabled = false;

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void TurnOn()
    {
        isOn = true;
        p1.Play();
        if (p2)
            p2.Play();
        Invoke("TurnOff", 10f);
    }

    public void TurnOff()
    {
       // GetComponent<SpriteRenderer>().enabled = false;
        p1.Stop();
        if (p2)
            p2.Stop();
        isOn = false;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player" && isOn)
        {
           // collision.gameObject.transform.position = pos.position;
            collision.gameObject.GetComponent<Rigidbody2D>().velocity = Vector3.zero;
            collision.gameObject.GetComponent<PlayerController>().TakeDamage(1);
            TurnOff();

        }
    }


}
