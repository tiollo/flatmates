﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartScreen : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        bool a = Input.GetButton("Action1") || Input.GetButton("Action2") || Input.GetKeyDown(KeyCode.G) || Input.GetKeyDown(KeyCode.Space);
        if (a)
            SceneManager.LoadScene("Game");
    }
}
