﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tv : MonoBehaviour
{
    public bool isOn = false;
    public Transform pos;

    // Start is called before the first frame update
    void Start()
    {
        GetComponent<SpriteRenderer>().enabled = false;

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void TurnOn()
    {
        GetComponent<SpriteRenderer>().enabled = true;
        isOn = true;
        Invoke("TurnOff", 10f);
    }

    public void TurnOff()
    {
        GetComponent<SpriteRenderer>().enabled = false;

        isOn = false;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag=="Player" && isOn)
        {
            collision.gameObject.transform.position = pos.position;
            collision.gameObject.GetComponent<Rigidbody2D>().velocity = Vector3.zero;
            collision.gameObject.GetComponent<PlayerController>().StopForAWhile();
            TurnOff();

        }
    }
}
